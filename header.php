<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Crop Service Center
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/bower_components/html5shiv/dist/html5shiv-printshiv.js"></script>
<![endif]-->
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'csc' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<div class="full-width-wrapper">

			<div class="site-wrapper">

				<div class="site-branding">
					<h1 class="site-title">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<!--[if lt IE 9]>
						    <img src="<?php echo get_template_directory_uri(); ?>/logo.jpg" />
						<![endif]-->
						<span class="screen-reader-text"><?php bloginfo( 'name' ); ?></span>
						</a>
					</h1>
				</div><!-- .site-branding -->

				<nav id="site-navigation" class="main-navigation" role="navigation">
					<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'csc' ); ?></button> -->
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => 'sf-menu' ) ); ?>
				</nav><!-- #site-navigation -->

			</div><!-- .site-wrapper -->

		</div><!-- .full-width-wrapper -->

		<?php if ( is_front_page() ) : ?>
			
			<?php $header_image = get_field( 'home_header_image' ); ?>
			
			<?php if ( ! empty( $header_image ) ) : ?>

					<img class="header-image site-wrapper" src="<?php echo $header_image['url']; ?>" />

			<?php endif; ?>

		<?php endif; ?>

	</header><!-- #masthead -->

	<div id="content" class="site-content site-wrapper">