/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens and enables tab
 * support for dropdown menus.
 */

jQuery(document).ready(function( $ ){
	
	$('ul.sf-menu').superfish({
			delay:       250,                             // 1/4 second delay on mouseout
			animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation
			speed:       'fast',                          // faster animation speed
		});

	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		}
	});

	$( '#input_1_1' ).on( 'focus', function() {

		$( '#field_1_2' ).slideDown();

	} );
	
	$('.gallery-icon').magnificPopup({
		delegate: 'a',
		type: 'image',
		gallery: {
	    	enabled: true
	    }
	});

	$('.gallery-item').on('click', function(){
		var child = $(this).find('.gallery-icon a');
		$(child).trigger('click');
	});

	$('.accordion').on('click', function(){
		$(this).toggleClass('open');
	});

});