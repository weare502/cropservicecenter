<?php
/**
 * Crop Service Center functions and definitions
 *
 * @package Crop Service Center
 */

if ( ! function_exists( 'csc_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function csc_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Crop Service Center, use a find and replace
	 * to change 'csc' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'csc', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'csc' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	// add_theme_support( 'post-formats', array(
	// 	'aside',
	// 	'image',
	// 	'video',
	// 	'quote',
	// 	'link',
	// ) );

	// Set up the WordPress core custom background feature.
	// add_theme_support( 'custom-background', apply_filters( 'csc_custom_background_args', array(
	// 	'default-color' => 'ffffff',
	// 	'default-image' => '',
	// ) ) );
}
endif; // csc_setup
add_action( 'after_setup_theme', 'csc_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function csc_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'csc_content_width', 800 );
}
add_action( 'after_setup_theme', 'csc_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function csc_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'csc' ),
		'id'            => 'sidebar-1',
		'description'   => 'This is placed in the right hand side of all pages.',
		'before_widget' => '<aside id="%1$s" class="widget clear %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'csc' ),
		'id'            => 'sidebar-2',
		'description'   => 'This appears in the left hand column of the footer.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'csc_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function csc_scripts() {

	wp_enqueue_style( 'csc-style', get_stylesheet_uri(), array(), '20150909' );

	wp_enqueue_style( 'superfish-style', get_template_directory_uri() . '/bower_components/superfish/dist/css/superfish.css' );

	wp_enqueue_script( 'superfish-js', get_template_directory_uri() . '/bower_components/superfish/dist/js/superfish.js', array( 'jquery' ), '20120713', true );

	wp_enqueue_script( 'superfish-hoverintent-js', get_template_directory_uri() . '/bower_components/superfish/dist/js/hoverIntent.js', array( 'jquery' ), '20120713', true );

	wp_enqueue_script( 'magnific-popup-js', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.js', array( 'jquery' ), '20120713', true );	

	wp_enqueue_script( 'csc-navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery' ), '20120713', true );

	wp_enqueue_script( 'csc-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
}
add_action( 'wp_enqueue_scripts', 'csc_scripts' );

/**
 * Implement the Custom Header feature.
 */
// require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
// require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// require get_template_directory() . '/inc/jetpack.php';



/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function csc_register_locations() {

	$labels = array(
		'name'                => __( 'Locations', 'csc' ),
		'singular_name'       => __( 'Location', 'csc' ),
		'add_new'             => _x( 'Add New Location', 'csc', 'csc' ),
		'add_new_item'        => __( 'Add New Location', 'csc' ),
		'edit_item'           => __( 'Edit Location', 'csc' ),
		'new_item'            => __( 'New Location', 'csc' ),
		'view_item'           => __( 'View Location', 'csc' ),
		'search_items'        => __( 'Search Locations', 'csc' ),
		'not_found'           => __( 'No Locations found', 'csc' ),
		'not_found_in_trash'  => __( 'No Locations found in Trash', 'csc' ),
		'parent_item_colon'   => __( 'Parent Location:', 'csc' ),
		'menu_name'           => __( 'Locations', 'csc' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'description'         => 'All of the locations operated by Crop Service Center',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-location-alt',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array( 'slug' => __( 'locations', 'csc' ) ),
		'capability_type'     => 'page',
		'supports'            => array(
			'title'
			)
	);

	register_post_type( 'csc_locations', $args );
}

add_action( 'init', 'csc_register_locations' );

add_action( 'admin_init', function(){
	add_editor_style( get_stylesheet_uri() );
});

function csc_filter_single_location_breadcrumb( $links ){
	
	// Remove current page
	$last = array_pop( $links );

	// Insert main Locations page
	$links[] = array( 
		'text' => __( 'Locations', 'csc' ),
		'url' => get_permalink( 9 ),
		'allow_html' => true
	);

	// Add current page back to array
	$links[] = $last;

	return $links;
}

function csc_yoast_breadcrumb() {

	if ( function_exists('yoast_breadcrumb') ) {

		if ( is_singular( 'csc_locations' ) ){
			add_filter( 'wpseo_breadcrumb_links', 'csc_filter_single_location_breadcrumb' );
		}

		$breadcrumbs = yoast_breadcrumb('<p id="breadcrumbs">','</p>' );

	}

}

add_action( 'gform_after_submission_1', function( $entry, $form ) {

	$number = $entry[1];
	$name = $entry["2.3"] . ' ' . $entry["2.6"];

	$number = str_replace( '(', '', $number );
	$number = str_replace( ') ', '', $number );
	$number = str_replace( '-', '', $number );

	global $table_prefix, $wpdb;

	$date = date('Y-m-d H:i:s' ,current_time('timestamp',0));

	$check_mobile = $wpdb->query($wpdb->prepare("SELECT * FROM `{$table_prefix}sms_subscribes` WHERE `mobile` = '%s'", $number ) );
	
	if( ! $check_mobile) {
	
		$check = $wpdb->insert(
			"{$table_prefix}sms_subscribes", 
			array(
				'date'		=> $date,
				'name'		=> $name,
				'mobile'	=> $number,
				'status'	=> '1', // active subscriber
				'group_ID'	=> 1, // Main SMS subscriber group
			)
		);

		global $sms;

		$sms->to = array( "+1{$number}" );

		$sms->msg = "{$name}, thanks for signing up for text alerts from Crop Service Center! We will never send you spam/junk mail. If you did not sign up for these alerts visit our website. LINK HERE";
		
		$result = $sms->SendSMS();


	} else {
		return false;
	}

}, 10, 2 );

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	//var_dump($buttons);
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Button',  
			'selector' => 'a',  
			'classes' => 'button',
			'icon'	   => ' fa fa-hand-pointer-o'
		),  
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' ); 

function csc_shortcode_format($content){
	$content = str_replace( '<p>[accordion]</p>', '[accordion]', $content );
	$content =  str_replace( '<p>[/accordion]</p>', '[/accordion]', $content );
	$content = str_replace('<p></p>', '', $content );
	return $content;
}

add_filter('the_content','csc_shortcode_format', 11 );

add_shortcode( 'accordion', function( $atts , $content = '' ){
	return "<div class='accordion'>" . $content . "</div>";
} );


function chromefix_inline_css()
{ 
	wp_add_inline_style( 'wp-admin', '#adminmenu { transform: translateZ(0); }' );
}
add_action('admin_enqueue_scripts', 'chromefix_inline_css');




