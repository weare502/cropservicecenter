<?php 

if ( have_rows( 'newsletters' ) ) : ?>

	<div class="newsletters">

	<?php while ( have_rows( 'newsletters' ) ) : the_row();
		$file = get_sub_field( 'file' );
	?>

		<div class="newsletter clear">

			<a href="<?php echo $file['url']; ?>" class="newsletter-link clear" target="_blank">

				<div class="right">
				
					<h3 class="newsletter-title"><?php the_sub_field( 'name' ); ?></h3>

					<p><?php the_sub_field( 'description' ); ?></p>

				</div>

				<img src="<?php echo $file['icon']; ?>" class="newsletter-icon" alt="Report Icon" />

			</a>

		</div>

	<?php endwhile; ?>
	
	</div><!-- End test-plot-reports -->

<?php endif; // newsletters