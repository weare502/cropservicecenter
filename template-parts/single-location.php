<?php if ( ! empty( get_sub_field( 'location_image' ) ) ) : ?>

	<img src="<?php echo get_sub_field( 'location_image' )['sizes']['large']; ?>" />

<?php endif; ?>

<?php $featured_image = get_field('location_featured_image');

if ( ! empty( $featured_image ) ) : ?>
	<img src="<?php echo $featured_image['sizes']['large']; ?>" class="featured-image" />
<?php endif; ?>

<?php the_field( 'location_description' ); ?>

<?php if ( have_rows( 'location_employees' ) ) : ?>

	<div class="employees clear">
	
		<?php while ( have_rows( 'location_employees' ) ) : the_row(); ?>

			<div class="employee clear">

				<?php if ( ! empty( get_sub_field( 'image' ) ) ) : ?>

					<img src="<?php echo get_sub_field( 'image' )['sizes']['medium']; ?>" />

				<?php endif; ?>

				<div class="wrapper clear">
					
					<h3 class="employee-name"><?php the_sub_field( 'name' ); ?> | <span class="employee-title"><?php the_sub_field( 'title' ); ?></span></h3>

					<?php the_sub_field( 'description' ); ?>

					<p class="contact">
						<span class="email"><i class="fa fa-envelope"></i>Email: <a href="mailto:<?php the_sub_field( 'email_address' ); ?>"><?php the_sub_field( 'email_address' ); ?></a></span>
						<span class="seperator"> | </span>
						<span class="phone"><i class="fa fa-phone"></i>Phone: <a href="tel:1<?php the_sub_field( 'phone_number' ); ?>"><?php the_sub_field( 'phone_number' ); ?></a></span>
					</p>

				</div>

			</div>

		<?php endwhile; ?>

	</div>

<?php endif; ?>

<?php if ( ! empty( get_field('location_image_gallery' ) ) ) : ?>

	<div class="popup-gallery clear">

		<h4 class="gallery-title">Photos from <?php the_title(); ?></h4>

		<?php $gallery = get_field('location_image_gallery' ); 
		
		foreach ( $gallery as $image ) : ?>

			<a href="<?php echo $image['url']; ?>" class="gallery-item third"><img src="<?php echo $image['sizes']['large']; ?>" /></a>

		<?php endforeach; ?>

	</div>

<?php endif; ?>