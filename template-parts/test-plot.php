<?php 

if ( have_rows( 'test_plot_reports' ) ) : ?>

	<div class="test-plot-reports">

	<?php while ( have_rows( 'test_plot_reports' ) ) : the_row();
		$file = get_sub_field( 'file' );
	?>

		<div class="report clear">

			<a href="<?php echo $file['url']; ?>" class="report-link clear" target="_blank">

				<div class="right">
				
					<h3 class="report-title"><?php the_sub_field( 'name' ); ?></h3>

					<p><?php the_sub_field( 'description' ); ?></p>

				</div>

				<img src="<?php echo $file['icon']; ?>" class="report-icon" alt="Report Icon" />

			</a>

		</div>

	<?php endwhile; ?>
	
	</div><!-- End test-plot-reports -->

<?php endif; // test_plot_reports