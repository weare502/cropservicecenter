<?php
/**
 * Template part for displaying single posts.
 *
 * @package Crop Service Center
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php csc_yoast_breadcrumb(); ?>

		<div class="entry-meta">

			<?php if ( ! is_singular( 'csc_locations' ) ) : ?>
				<?php csc_posted_on(); ?>
			<?php endif; ?>
		
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php if ( is_singular( 'csc_locations' ) ) : ?>

			<?php require( 'single-location.php' ); ?>

		<?php endif; ?>

		<?php the_content(); ?>
		<?php
			// wp_link_pages( array(
			// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'csc' ),
			// 	'after'  => '</div>',
			// ) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php csc_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

