<?php
/**
 * This template shows the different locations.
 *
 * @package Crop Service Center
 */

/**
 * The WordPress Query class.
 * @link http://codex.wordpress.org/Function_Reference/WP_Query
 *
 */
$args = array(
	//Type & Status Parameters
	'post_type'   => 'csc_locations',
	
	//Order & Orderby Parameters
	'order'               => 'ASC',
	'orderby'             => 'title',

	//Pagination Parameters
	'posts_per_page'         => -1,
);

$locations_query = new WP_Query( $args );

if ( $locations_query->have_posts() ) : ?>

	<div class="locations-list clear">

		<?php while ( $locations_query->have_posts() ) : $locations_query->the_post(); ?>

			<?php $thumb = get_field( 'location_image'); ?>

			<div class="locations-list-item clear">

				<a href="<?php the_permalink(); ?>">

					<div class="hover-wrapper">

						<div class="bottom-right"></div>

						<img src="<?php echo $thumb['sizes']['medium']; ?>" alt="<?php echo $thumb['alt']; ?>" />

					</div>

					<h3 class="location-title"><?php the_title(); ?></h3>

				</a>

			</div>

		<?php endwhile; ?>

	</div>

<?php endif; ?>

<?php wp_reset_postdata();