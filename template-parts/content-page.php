<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Crop Service Center
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( ! is_front_page() ) : ?>

		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php csc_yoast_breadcrumb(); ?>
		</header><!-- .entry-header -->
		
	<?php endif; ?>

	<div class="entry-content">

		<?php 

			if ( is_page( 9 ) ) { // Locations Page
				require( 'locations.php' );
			}

			the_content();

			if ( is_front_page() ) { // Runs on the Home Page Only
				require( 'home-page.php' );
			} 

			if ( is_page( 13 ) ) {
				require( 'test-plot.php' );
			}

			if ( is_page( 261 ) ){
				require( 'newsletters.php' );
			}
		
		?>

		<?php
			// wp_link_pages( array(
			// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'csc' ),
			// 	'after'  => '</div>',
			// ) );
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( esc_html__( 'Edit', 'csc' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

