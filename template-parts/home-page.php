<?php the_field( 'home_content' ); ?>

<?php if ( have_rows( 'home_image_links' ) ) : ?>
	
	<div class="images-with-links">
	
	<?php while ( have_rows( 'home_image_links' ) ) : the_row(); ?>
		
		<?php $link_image = get_sub_field( 'image' ); ?>

		<a href="<?php the_sub_field('link'); ?>" class="image-link">
			<img src="<?php echo $link_image['sizes']['medium']; ?>" />
		</a>

	<?php endwhile; ?>

	</div>

<?php endif; ?>