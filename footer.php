<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Crop Service Center
 */

?>

	</div><!-- #content --><!-- .site-wrapper -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="site-wrapper">

			<div class="one-third">
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</div>

			<?php echo do_shortcode( '[gravityform id="1" title="true" description="false" ajax="true" tabindex="40"]' ); ?>

			<?php echo do_shortcode( '[gravityform id="2" title="true" description="false" ajax="true" tabindex="70"]' ); ?>

		</div><!-- .site-wrapper -->

		<div class="site-info clearfix">
			&copy; 1983 - <?php echo date('Y'); ?> Crop Service Center. <br> All Rights Reserved.
		</div><!-- .site-info -->

	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'PT+Sans:400,700:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); </script>

</body>
</html>
