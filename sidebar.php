<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Crop Service Center
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary">

	<?php dynamic_sidebar( 'sidebar-1' ); ?>

	<div class="market-data">
		<a href="http://agnews.dtn.com/index.cfm?show=42&cid=773&sid=1&mkt=579"><i class="fa fa-line-chart">&nbsp;</i>Market Data &amp; Futures</a>
	</div>
</div><!-- #secondary -->